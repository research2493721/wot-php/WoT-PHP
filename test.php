<?php

namespace Wings;

use malkusch\autoloader\Autoloader;
use React;
use Wings\src\FileSystem;


require __DIR__ . '/../../php-autoloader-1.14.4/autoloader.php';

require __DIR__ . '/../../include/vendor/autoload.php';

FileSystem::requireRecursive("./vendor/src/");


// Use the PHP autoloader only for importing own files (Wing)
$autoloader = new Autoloader(__DIR__);
$autoloader->register();

$message = new \Wings\Network\Message\Internal\Connection\PingPong\Ping();
$parser = new \Wings\Network\Parser();


// Set specific message require attributes
$message->setSource("192.168.1.2");

$messageRawData = $message->getRawData();

var_dump($messageRawData);

$message = new \Wings\Network\Message\Internal\Connection\PingPong\Ping();

$message->buildFromArray(json_decode($messageRawData, true));

var_dump($message->getRawData());

