<?php

namespace Wings\Config;


interface ConfigurableInterface {

    public function setConfig(array $config);

    public function getConfig();
}