<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 02/06/15
 * Time: 12:34
 */

namespace Wings\Network;


use Wings\Network\Message;

class Parser
{

    public function parseFromArray(array $array, $jsonString = null)
    {
        if (!array_key_exists(Message::MSG_TYPE_KWORD, $array)) {
            throw new \Exception("Message can't be parsed");
        }

        if ($jsonString === null) {
            $jsonString = json_encode($array);
        }

        $msgType = $array[Message::MSG_TYPE_KWORD];

        $message = new Message\Unknown();

        switch($msgType) {
            case Message::MSG_TYPE:
                $message = new Message();
                break;
            case Message\Internal\Connection\PingPong\Ping::MSG_TYPE:
                $message = new Message\Internal\Connection\PingPong\Ping();
                break;
            case Message\Internal\Connection\PingPong\Pong::MSG_TYPE:
                $message = new Message\Internal\Connection\PingPong\Pong();
                break;
            default:
                return $message;
        }

        $message->buildFromArray($array);

        return $message;
    }

    public function parseFromJsonString($jsonString)
    {

        $array = json_decode($jsonString, true);

        return $this->parseFromArray($array, $jsonString);

    }

    /**
     * @param array|string $rawMessage
     * @return bool
     */
    public function isParseable($rawMessage)
    {

        $array = $rawMessage;

        if (is_string($rawMessage)) {
            $array = json_decode($rawMessage, true);
        }
        return array_key_exists(Message::MSG_TYPE_KWORD, $array);
    }

}