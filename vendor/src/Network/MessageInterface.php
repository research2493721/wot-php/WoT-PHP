<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 26/05/15
 * Time: 11:38
 */

namespace Wings\Network;


interface MessageInterface
{

    /**
     * Sets the source of the message.
     * @return mixed
     */
    public function getSource();

    /**
     * Gets the source of the message.
     * @param mixed $source
     */
    public function setSource($source);

    /**
     * Gets the message type.
     * @return mixed
     */
    public function getType();

    /**
     * Sets the message type.
     * @param $type
     * @return mixed
     */
    public function setType($type);

    /**
     * Gets the raw data of the message.
     * @return mixed
     */
    public function getRawData();

    /**
     * Sets the raw data of the message.
     * @param mixed $rawData
     */
    public function setRawData($rawData);

    /**
     * Builds the message from an array.
     * @param $array
     * @return mixed
     */
    public function buildFromArray($array);

}