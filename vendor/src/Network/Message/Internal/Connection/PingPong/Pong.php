<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 02/06/15
 * Time: 12:16
 */

namespace Wings\Network\Message\Internal\Connection\PingPong;


use Wings\Network\Message;
use Wings\Network\Message\Internal\Connection\PingPong;

class Pong extends PingPong
{

    const MSG_TYPE = 'pong';

    protected $verb = 'POST';

    protected $uri = 'pong';

}