<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 26/05/15
 * Time: 12:29
 */

namespace Wings\Network\Message;


use Wings\Network\Message;

/**
 * Class WebSocketConnectionState. Response after a WebSocket connection has been established.
 * @package Wings\Network\Message
 */
class WebSocketConnectionState extends Message
{

    const MSG_STATE_KEYWORD = 'state';

    protected $connectionStateCode;

    protected $connectionStateValue;

    /**
     * @return mixed
     */
    public function getConnectionStateValue()
    {
        return $this->connectionStateValue;
    }

    /**
     * @param $connectionStateValue
     * @return WebSocketConnectionState
     */
    public function setConnectionStateValue($connectionStateValue)
    {
        $this->connectionStateValue = $connectionStateValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConnectionStateCode()
    {
        return $this->connectionStateCode;
    }

    /**
     * @param $connectionStateCode
     * @return WebSocketConnectionState
     */
    public function setConnectionStateCode($connectionStateCode)
    {
        $this->connectionStateCode = $connectionStateCode;
        return $this;
    }

    protected function buildRawData()
    {
        $array = parent::buildRawData();

        $array["status"] = $this->getConnectionStateCode();
        $array["data"] = array(
            "type" => self::MSG_STATE_KEYWORD,
            "value" => $this->getConnectionStateValue()
        );

        return $array;

    }

}