<?php

namespace Wings\Network\Message;


use Wings\Network\Message;

/**
 * Class WebSocketConnectionResponse. Response to a WebSocket connection request.
 * @package Wings\Network\Message
 */
class WebSocketConnectionResponse extends Message
{

    /**
     * @var string
     */
    protected $connectionHost;

    /**
     * @var string
     */
    protected $connectionPort;

    /**
     * @var string
     */
    protected $connectionUri;

    /**
     * @var string
     */
    protected $connectionVerb;

    /**
     * @return string
     */
    public function getConnectionHost()
    {
        return $this->connectionHost;
    }

    /**
     * @param $connectionHost
     * @return WebSocketConnectionResponse
     */
    public function setConnectionHost($connectionHost)
    {
        $this->connectionHost = $connectionHost;

        return $this;
    }

    /**
     * @return string
     */
    public function getConnectionPort()
    {
        return $this->connectionPort;
    }

    /**
     * @param $connectionPort
     * @return WebSocketConnectionResponse
     */
    public function setConnectionPort($connectionPort)
    {
        $this->connectionPort = $connectionPort;

        return $this;
    }

    /**
     * @return string
     */
    public function getConnectionUri()
    {
        return $this->connectionUri;
    }

    /**
     * @param $connectionUri
     * @return WebSocketConnectionResponse
     */
    public function setConnectionUri($connectionUri)
    {
        $this->connectionUri = $connectionUri;

        return $this;
    }

    /**
     * @return string
     */
    public function getConnectionVerb()
    {
        return $this->connectionVerb;
    }

    /**
     * @param $connectionVerb
     * @return WebSocketConnectionResponse
     */
    public function setConnectionVerb($connectionVerb)
    {
        $this->connectionVerb = $connectionVerb;

        return $this;
    }


    protected function buildRawData()
    {
        $array = parent::buildRawData();

        $array["addr"] = array(
            "host" => $this->getConnectionHost(),
            "port" => $this->getConnectionPort()
        );
        $array["v"] = $this->getConnectionVerb();
        $array["uri"] = $this->getConnectionUri();

        return $array;

    }

}