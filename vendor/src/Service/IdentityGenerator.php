<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 21/04/15
 * Time: 13:02
 */

namespace Wings\Service;

class IdentityGenerator {

    /**
     * @var \Math_BigInteger
     */
    protected $identity;

    public function __construct() {
        $this->identity = new \Math_BigInteger(0);
    }

    public function next() {
        $this->identity = $this->identity->add(new \Math_BigInteger(1));
        if ($this->identity->compare(new \Math_BigInteger("100000")) == 0) {
            $this->identity =  $this->identity = new \Math_BigInteger(0);
        }
        return $this->identity->toString();
    }
}