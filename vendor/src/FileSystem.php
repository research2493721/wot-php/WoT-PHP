<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 02/06/15
 * Time: 18:15
 */

namespace Wings\src;


class FileSystem {

    static public function requireRecursive($path)
    {
        $directory = new \RecursiveDirectoryIterator($path);
        $recIterator = new \RecursiveIteratorIterator($directory);
        $regex = new \RegexIterator($recIterator, '/\/*.php$/i');

        foreach($regex as $item) {
            //echo $item . PHP_EOL;
            include_once($item->getPathname());
        }
    }

}