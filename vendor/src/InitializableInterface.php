<?php

namespace Wings;


interface InitializableInterface {

    public function initialize();

}