<?php

namespace Wings\Actor;


class RemoteActorRef
    extends LocalActorRef
{

    protected $remoteAddress;

    protected $remoteRouter;

    public function getRemoteAddress()
    {
        return $this->remoteAddress;
    }

    public function setRemoteAddress($remoteAddress)
    {
        $this->remoteAddress = $remoteAddress;
    }


    public function getRemoteRouter()
    {
        return $this->remoteRouter;
    }

    public function setRemoteRouter($remoteRouter)
    {
        $this->remoteRouter = $remoteRouter;
    }

}