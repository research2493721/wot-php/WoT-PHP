<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 08/06/15
 * Time: 14:33
 */

namespace Wings\Server;


use Wings\Exception;
use Wings\Server\Local;

class Loader
{

    protected $servers = array();

    public function loadServer($name, $path)
    {
        $this->servers[$name] = $path;
    }

    public function loadServers(array $servers)
    {
        array_merge($this->$servers, $servers);
    }

    public function initializeServers()
    {
        $servers = $this->servers;
        foreach ($servers as $server) {
            if ($server instanceof Local) {
                $server->initialize();
            } else {
                throw new Exception('The loader tried to initialize an object that is not a server');
            }
        }
    }

}