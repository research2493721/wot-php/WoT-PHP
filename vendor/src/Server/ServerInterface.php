<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 08/06/15
 * Time: 17:21
 */

namespace Wings\Server;


interface ServerInterface
{

    public function setAddress($address);

    public function getAddress();

    public function setPort($port);

    public function getPort();

    public function setNetworkManager(\Wings\Network\Manager $networkManager);

    public function getNetworkManager();

    public function tell(\Wings\Network\MessageInterface $message);

}