<?php
use \UnitTester;

class MessageCest
{
    public function _before(UnitTester $I)
    {
    }

    public function _after(UnitTester $I)
    {
    }

    // tests
    public function messageRebuild(UnitTester $I)
    {

        $message = new \Wings\Network\Message();
        $parser = new \Wings\Network\Parser();

        $I->wantToTest("that ".get_class($message)." can be built from raw data of the same message");

        $messageRawData = $message->getRawData();

        $newMessage = $parser->parseFromJsonString($messageRawData);

        $I->expect("that calling getRawData() on both messages return the same");

        $I->assertEquals($newMessage->getRawData(), $message->getRawData());

        $I->expect("that both messages are an instance of the same class");

        $I->assertEquals(get_class($newMessage), get_class($message));

    }

    public function allMessagesRebuild(UnitTester $I)
    {

        $message = new \Wings\Network\Message\Internal\Connection\PingPong\Ping();
        $parser = new \Wings\Network\Parser();

        $I->wantToTest("that ".get_class($message)." can be built from raw data of the same message");

        // Set specific message require attributes
        $message->setSource("192.168.1.2");
        $message->setUri("ping");
        $message->setVerb("POST");

        $messageRawData = $message->getRawData();

        $newMessage = $parser->parseFromJsonString($messageRawData);

        $I->expect("that calling getRawData() on both messages return the same");

        $I->assertEquals($message->getRawData(), $newMessage->getRawData());

        $I->expect("that both messages are an instance of the same class");

        $I->assertEquals(get_class($message), get_class($newMessage));

    }


}