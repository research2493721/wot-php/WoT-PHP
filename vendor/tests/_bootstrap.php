<?php
// This is global bootstrap for autoloading

require __DIR__.'/../../../../php-autoloader-1.14.4/autoloader.php';

use malkusch\autoloader\Autoloader;

$autoloader = new Autoloader(__DIR__.'/../src');
$autoloader->register();

\Wings\src\FileSystem::requireRecursive(__DIR__.'/../src');

