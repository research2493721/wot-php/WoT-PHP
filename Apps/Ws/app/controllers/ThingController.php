<?php

namespace Wings\Apps\Ws\Controllers;


use Phalcon\Mvc\Controller;

class ThingController extends Controller
{

    public function queryAction()
    {
        $phalconResponse = new \Phalcon\Http\Response();

        $phalconResponse->setContent(json_encode(
            array(
                "status" => 200,
                "data" => array(
                    "type" => "real",
                    "value" => "25",
                    "unit" => "ºC"
                )
            )
        ));

        return $phalconResponse;
    }

}