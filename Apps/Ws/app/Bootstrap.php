<?php

namespace Wings\Apps\Ws;

use Phalcon\Mvc\Application;

class Bootstrap
{
    public function getApplication()
    {

        /**
         * Read the configuration
         */
        $config = include __DIR__ . "/config/config.php";

        /**
         * Read auto-loader
         */
        include __DIR__ . "/config/loader.php";

        /**
         * Read services
         */
        $di = include __DIR__ . "/config/services.php";

        $application = new Application();
        $application->setDI($di);

        return $application;
    }
}