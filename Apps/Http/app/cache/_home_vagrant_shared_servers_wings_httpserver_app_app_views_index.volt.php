<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Phalcon PHP Framework</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->url->get('public/css/web_css.css'); ?>">


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input placeholder="Email" class="form-control" type="text">
                </div>
                <div class="form-group">
                    <input placeholder="Password" class="form-control" type="password">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1><?php echo $main_header; ?></h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more »</a></p>
    </div>
</div>

<div class="container">
    <div class="row">
        <?php echo $this->getContent(); ?>
    </div>

    <hr>

    <footer class="row">
        <div class="large-12 columns">
            <hr>
            <div class="row">
                <div class="large-5 columns">
                    <p>&copy; Copyright no one at all. Go to town.</p>
                </div>
                <div class="large-7 columns">
                    <ul class="inline-list right">
                        <li><a href="#">Section 1</a></li>
                        <li><a href="#">Section 2</a></li>
                        <li><a href="#">Section 3</a></li>
                        <li><a href="#">Section 4</a></li>
                        <li><a href="#">Section 5</a></li>
                        <li><a href="#">Section 6</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div> <!-- /container -->
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="<?php echo $this->url->get('public/js/web_js.js'); ?>"></script>

<script>
</script>
<?php if ($partial_js) { ?>
    <?php echo $this->partial($partial_js); ?>
<?php } ?>
</body>
</html>