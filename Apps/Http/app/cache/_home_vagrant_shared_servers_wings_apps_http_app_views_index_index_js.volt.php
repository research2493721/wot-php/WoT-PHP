<script type="text/javascript">

    ws = {ws: undefined, status: 0};

    function startWs(msg) {
        if ("WebSocket" in window) {
            var url = msg.addr.host + ":" + msg.addr.port + "/" + msg.v + "/" + msg.uri;
            //url = msg.addr.host + ":" + msg.addr.port + "/raw/echo";
            ws.ws = new WebSocket(url);

            ws.ws.onopen = function () {
                // alert('Connected to WsServer');
                $(".search-ws").fadeIn("slow");
                $(".request-ws-connection").fadeOut("slow");
            };

            ws.ws.onmessage = function (e) {
                var mainQueryResponseDisplay = $("#main-query-response");
                var mainQueryResponseText = mainQueryResponseDisplay.find("p");
                var data = JSON.parse(e.data);

                mainQueryResponseDisplay.hide();

                mainQueryResponseText.removeClass();

                switch (data.status) {
                    case 200:
                        mainQueryResponseText.addClass("text-success");
                        switch (data.data.type) {
                            case "status":
                                mainQueryResponseText.text("Connection established");
                                break;
                            case "real":
                                mainQueryResponseText.text("The answer is: " + data.data.value + data.data.unit);
                        }
                        break;
                }


                mainQueryResponseDisplay.fadeIn("fast");

                console.log('Received From Apps: ' + e.data); //log the received message
            };

            ws.ws.onerror = function (error) {
                console.log('Error Logged: ' + error); //log errors
            };

        }
        else {
            // The browser doesn't support WebSocket
            alert("WebSocket NOT supported by your Browser!");
        }
    }

    function sendQuery() {
        var query = {
            "uri": "/GET/resource/thing/query",
            "query": {"what": "temperature", "loc": "Carrer de Sants"}
        }
        ws.ws.send(JSON.stringify(query));
    }

    function requestWsConnection() {
        var request = $.ajax({
            url: "<?php echo $url_request_conn_ws; ?>",
            method: "GET"
        });

        request.done(function (msg) {
            console.debug(msg);
            startWs(msg);
        });
    }

    $(".request-ws-connection").click(function (event) {
        requestWsConnection();
    });

    $(".search-ws").hide();

    $(".search-ws-btn").click(function (event) {
        sendQuery();
    });


</script>