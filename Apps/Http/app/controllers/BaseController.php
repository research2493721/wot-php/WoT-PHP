<?php
/**
 * Created by PhpStorm.
 * User: Work
 * Date: 30/04/15
 * Time: 11:19
 */

namespace Wings\Apps\Http\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class BaseController extends Controller
{
    /**
     * @var \Guzzle\Http\Message\Response
     */
    protected $response;

    /**
     * @var \Guzzle\Http\Message\Request
     */
    protected $request;

    public function beforeExecuteRoute(Dispatcher $dispatcher) {
        $this->initialize();
    }

    public function initialize()
    {
        $logger = $this->getDI()->get("logger");
        $logger->log("Initializing base controller");
        $this->response = $this->getDI()->get('response');
        $this->request = $this->getDI()->get('request');
    }

    protected function response() {

        $phalconResponse = new \Phalcon\Http\Response;

        $phalconResponse->setContent($this->response->getMessage());

        return $phalconResponse;
    }
}