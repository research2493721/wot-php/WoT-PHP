<?php

namespace Wings;

use malkusch\autoloader\Autoloader;
use React;


require __DIR__.'/../../php-autoloader-1.14.4/autoloader.php';

require __DIR__.'/../../include/vendor/autoload.php';

// Use the PHP autoloader only for importing own files (Wing)
$autoloader = new Autoloader(__DIR__);
$autoloader->register();

/**
 * Tests
 */

$loop = React\EventLoop\Factory::create();

$context = new React\ZMQ\Context($loop);

// Apps
$server = $context->getSocket(\ZMQ::SOCKET_ROUTER);
$bindEndpoint = 'inproc://*:5556';
$connectEndpoint = 'inproc://127.0.0.1:5556';
$server->setSockOpt(\ZMQ::SOCKOPT_IDENTITY, $connectEndpoint);
$server->bind($bindEndpoint);
echo 'Service is ready at '. $bindEndpoint. PHP_EOL;


$server->on('messages', function($msg) use ($server) {
    echo 'Router messages'. PHP_EOL;
    var_dump($msg);
});

$server->on('message', function($msg) {
    echo 'Router message'. PHP_EOL;
    var_dump($msg);
});

// Client
$client = $context->getSocket(\ZMQ::SOCKET_ROUTER);
$endpoint = 'inproc://127.0.0.1:5556';
$client->connect($endpoint);
$client->send(array('CONNECT', $endpoint));
sleep(1);
$client->send(array('REQUEST', 'HOLA'));


$client->on('error', function ($e) {
    var_dump($e->getMessage());
});

$client->on('messages', function($msg) use ($client) {
    echo 'Dealer messages'. PHP_EOL;
    var_dump($msg);
});

$client->on('message', function($msg) use ($client) {
    echo 'Dealer message'. PHP_EOL;
    var_dump($msg);
});

$loop->addPeriodicTimer(1, function (React\EventLoop\Timer\Timer $timer) {
    echo 'Loop working'. PHP_EOL;
});


$loop->run();