<?php

namespace Wings;

use malkusch\autoloader\Autoloader;
use Phalcon\Di;
use Phalcon\Events;
use React;
use Wings\Network\Message\Internal\Connection\PingPong\Ping;
use Wings\Network;


require __DIR__.'/../../php-autoloader-1.14.4/autoloader.php';

require __DIR__.'/../../include/vendor/autoload.php';

// Use the PHP autoloader only for importing own files (Wing)
$autoloader = new Autoloader(__DIR__);
$autoloader->register();

/**
 * Tests
 */

$loop = React\EventLoop\Factory::create();

$context = new React\ZMQ\Context($loop);

$worker = $context->getSocket(\ZMQ::SOCKET_DEALER);

$loop->addPeriodicTimer(1, function (React\EventLoop\Timer\Timer $timer) use ($worker) {
    echo 'Connecting'. PHP_EOL;
    $worker->setSockOpt(\ZMQ::SOCKOPT_IDENTITY, 'A');
    $worker->connect('tcp://127.0.0.1:5556');
    $timer->getLoop()->cancelTimer($timer);     // Cancel the timer after connecting
});

$loop->addPeriodicTimer(2, function (React\EventLoop\Timer\Timer $timer) use ($worker) {
    echo 'Dealer sending messages with an interval of 2 seconds'. PHP_EOL;
    $message = new Ping();
    $message->setSource('a');
    $worker->send(array($message->getRawData()));
    //$timer->getLoop()->cancelTimer($timer);     // Cancel the timer after connecting
});

$worker->on('error', function ($e) {
    var_dump($e->getMessage());
});

$worker->on('messages', function($msg) use ($worker) {
    echo 'Dealer messages'. PHP_EOL;
    var_dump($msg);
});

$worker->on('message', function($msg) use ($worker) {
    echo 'Dealer message'. PHP_EOL;
    var_dump($msg);
});

$router = $context->getSocket(\ZMQ::SOCKET_ROUTER);
$router->bind('tcp://127.0.0.1:5556');

$ZMQNetworkManager = new Network\Manager\ZMQ();

$ZMQNetworkManager->setRouter($router);

$ZMQDi = new Di();

$ZMQEm = new Events\Manager();

$ZMQNetworkManager->setEventsManager($ZMQEm);
$ZMQDi->set('logger', new \Phalcon\Logger\Adapter\File("./ZMQNetworkManager.log"));
$ZMQDi->set('message_parser', new Network\Parser());
$ZMQEm->attach('message:internal', function () use ($ZMQDi) {
    $ZMQDi->get('logger')->log('Event!');
});

$loop->addPeriodicTimer(2, function (React\EventLoop\Timer\Timer $timer) use ($router) {
    echo 'Router sending messages with an interval of 2 seconds'. PHP_EOL;
    $router->send(array('A', 'END'));
});

$loop->run();