<?php

define('BASE_PATH', realpath(dirname(__FILE__)));

define('SERVER_PATH', BASE_PATH. "/servers/");

return array(
    'base_path' => __DIR__,
    'server_path' => __DIR__. "/servers/"
);